#ifndef __LIE_UTILS_HPP
#define __LIE_UTILS_HPP

#include "matplotlib-cpp/matplotlibcpp.h"
namespace plt = matplotlibcpp;

#include <Eigen/Eigenvalues>


namespace LieUtils
{


void plot_frame(double x, double y, double angle=0.0,
                std::string color_x="r", std::string color_y="b")
{
  double PI = 3.14159265359;  
  
  plt::quiver(std::vector<double>({x}), std::vector<double>({y}), std::vector<double>({cos(angle)}), std::vector<double>({sin(angle)}), std::map<std::string, std::string>({{"color", color_x}, {"edgecolor", "k"}, {"linewidth", "1"}}));
  plt::quiver(std::vector<double>({x}), std::vector<double>({y}), std::vector<double>({cos(angle+PI/2.0)}), std::vector<double>({sin(angle+PI/2.0)}), std::map<std::string, std::string>({{"color", color_y}, {"edgecolor", "k"}, {"linewidth", "1"}}));  
}

void plot_frame(const Lie::SE3& pose, std::string color_x="r", std::string color_y="b")
{
  plot_frame(pose.t()(0),
             pose.t()(1),
             atan2(pose.R()()(1,0), pose.R()()(0,0)),
             color_x,
             color_y);
}


void plot_se3_elements_2d(std::vector<Lie::SE3> frames, std::string color="r", const std::string& name="", bool use_scatter=false)
{
  std::vector<double> x; x.reserve(frames.size());
  std::vector<double> y; y.reserve(frames.size());
  std::vector<double> u; y.reserve(frames.size());
  std::vector<double> v; v.reserve(frames.size());

  for(auto f : frames)
  {
    x.push_back(f.t()(0));
    y.push_back(f.t()(1));

    double angle = atan2(f.R()()(1,0), f.R()()(0,0));
    u.push_back(cos(angle));
    v.push_back(sin(angle));
  }

  
  if(use_scatter)
  {
    plt::scatter(x, y, 3, "k");
  }
  else
  {
    if(name.size())
    {
      plt::named_quiver(name, x,y,u,v, std::map<std::string, std::string>({{"color", color}}));
    }
    else
    {
      plt::quiver(x,y,u,v, std::map<std::string, std::string>({{"color", color}}));
    }
  }
}

template <typename T>
std::vector<size_t> sort_indices(const std::vector<T> &v) {

  // initialize original index locations
  std::vector<size_t> idx(v.size());
  iota(idx.begin(), idx.end(), 0);

  // sort indexes based on comparing values in v
  sort(idx.begin(), idx.end(),
       [&v](size_t i1, size_t i2) {return v[i1] > v[i2];});

  return idx;
}

std::pair<Eigen::ArrayXXd, Eigen::ArrayXXd>
mesh_grid(Eigen::VectorXd x, Eigen::VectorXd y)
{
  int N = x.rows();
  int M = y.rows();
  auto x_coords = x.transpose().replicate(M, 1);
  auto y_coords = y.replicate(1, N);
  
  return std::make_pair(x_coords.array(), y_coords.array());
}

double Sin(double x) { return sin(x); }
double Cos(double x) { return cos(x); }

Eigen::MatrixXd
flattened_mesh_grid(const Eigen::MatrixXd& X, const Eigen::VectorXd& y)
{
  int N = X.rows();
  int M = y.rows();

  Eigen::MatrixXd X_rep = X.replicate(M, 1);
  Eigen::MatrixXd Y_rep = y.transpose().replicate(N,1);

  Eigen::MatrixXd XY(N*M, X.cols() + 1);
  XY << X_rep, Eigen::Map<Eigen::VectorXd>(Y_rep.data(), N*M);

  return XY;
}

Eigen::MatrixXd
generalized_flattened_mesh_grid(std::vector<Eigen::VectorXd> coords)
{
  Eigen::MatrixXd Replicated(coords[0].rows(), 1);
  Replicated << coords[0];
  for(int i=1;i<coords.size();i++)
  {
    Replicated = flattened_mesh_grid(Replicated, coords[i]);
  }

  return Replicated;
}

template <typename StateT>
void plot_flattened_sigma_error_ellipsoid_5dsphere(const Lie::UncertainGroupElementRef<Lie::SE3, StateT>& pose, double sigma=3.0, std::string color="k", std::map<std::string, std::string> keywords={}, std::string name="", int N=20, int offset=5)
{
  double PI = 3.14159;
  //  int N = 20;

  // 5-Sphere Coords
  Eigen::VectorXd phi_1 = Eigen::VectorXd::LinSpaced(N, 0, PI);
  Eigen::VectorXd phi_2 = Eigen::VectorXd::LinSpaced(N, 0, PI);
  Eigen::VectorXd phi_3 = Eigen::VectorXd::LinSpaced(N-offset, 0, PI);
  Eigen::VectorXd phi_4 = Eigen::VectorXd::LinSpaced(N-offset, 0, PI);
  Eigen::VectorXd phi_5 = Eigen::VectorXd::LinSpaced(N, 0, 2*PI);
  std::vector<Eigen::VectorXd> coords = {phi_1, phi_2, phi_3, phi_4, phi_5};
  Eigen::MatrixXd grid = generalized_flattened_mesh_grid(coords);

  int total_size = phi_1.rows()*phi_2.rows()*phi_3.rows()*phi_4.rows()*phi_5.rows();
  
  Eigen::MatrixXd sphere(6, total_size);

  for(int i=0;i<grid.rows();i++)
  {
    Eigen::VectorXd phi = grid.row(i).transpose();
    Eigen::VectorXd xi = Eigen::VectorXd::Zero(6);
    xi(0) = cos(phi(0));
    xi(1) = sin(phi(0))*cos(phi(1));
    xi(2) = sin(phi(0))*sin(phi(1))*cos(phi(2));
    xi(3) = sin(phi(0))*sin(phi(1))*sin(phi(2))*cos(phi(3));
    xi(4) = sin(phi(0))*sin(phi(1))*sin(phi(2))*sin(phi(3))*cos(phi(4));
    xi(5) = sin(phi(0))*sin(phi(1))*sin(phi(2))*sin(phi(3))*sin(phi(4));
    sphere.col(i) = xi;
  }

  Eigen::MatrixXd cov = pose.marginal_cov();

  Eigen::LLT<Eigen::MatrixXd> lltOfCov(cov);
  auto L = lltOfCov.matrixL();

  std::vector<double> ellipsoid_x;
  ellipsoid_x.reserve(total_size);
  std::vector<double> ellipsoid_y;
  ellipsoid_y.reserve(total_size);
  std::vector<double> ellipsoid_z;
  ellipsoid_z.reserve(total_size);  

  sphere = L*sphere;
  sphere = sigma*sphere;

  for(int i=0;i<total_size;i++)
  {
    Eigen::VectorXd xi = sphere.col(i);
    
    auto temp = Lie::compose( Lie::SE3::Exp(xi), pose.mu() );
    Eigen::VectorXd xyz = temp.t();
    ellipsoid_x.push_back(xyz(0));
    ellipsoid_y.push_back(xyz(1));
    ellipsoid_z.push_back(xyz(2));    
  }

  if(name.size())
  {
    //    std::cout << "HERE: " << name << "\n";
    if(keywords.size()){
      plt::named_plot(name, ellipsoid_x, ellipsoid_y, color, keywords);
    }
    else
    {
      plt::named_plot(name, ellipsoid_x, ellipsoid_y, color);
    }
  }
  else
  {
    if(keywords.size()){    
      plt::plot(ellipsoid_x, ellipsoid_y, color, keywords);
    }
    else
    {
      plt::plot(ellipsoid_x, ellipsoid_y, color);
    }
  }
}

template <typename StateT>
void plot_flattened_sigma_error_ellipsoid_5dsphere(const Lie::UncertainGroupElementRef<Lie::SE3, StateT>& pose, double sigma, std::string color, std::string name)
{
  plot_flattened_sigma_error_ellipsoid_5dsphere(pose, sigma, color, {}, name);
}



template <typename StateT>
void plot_flattened_sigma_error_ellipsoid_3dsphere(const Lie::UncertainGroupElementRef<Lie::SE3, StateT>& pose, double sigma=3.0, std::string color="k", std::map<std::string, std::string> keywords={}, std::string name="")
{
  double PI = 3.14159;
  int N = 1000;
  int M = 1000;
  Eigen::VectorXd phi = Eigen::VectorXd::LinSpaced(N, -PI, PI);
  Eigen::VectorXd theta = Eigen::VectorXd::LinSpaced(M, -PI/2.0, PI/2.0);
  auto grid = mesh_grid(theta, phi);
  Eigen::MatrixXd X_sph = (grid.first.unaryExpr(&Cos) * grid.second.unaryExpr(&Cos)).matrix();
  Eigen::MatrixXd Y_sph = (grid.first.unaryExpr(&Cos) * grid.second.unaryExpr(&Sin)).matrix();
  Eigen::MatrixXd Z_sph = (grid.first.unaryExpr(&Sin)).matrix();

  Eigen::RowVectorXd X = Eigen::Map<const Eigen::RowVectorXd>(X_sph.data(), N*M);
  Eigen::RowVectorXd Y = Eigen::Map<const Eigen::RowVectorXd>(Y_sph.data(), N*M);
  Eigen::RowVectorXd Z = Eigen::Map<const Eigen::RowVectorXd>(Z_sph.data(), N*M);  
  
  Eigen::MatrixXd cov = pose.marginal_cov();

  Eigen::LLT<Eigen::MatrixXd> lltOfCov(cov);
  auto L = lltOfCov.matrixL();

  std::vector<double> ellipsoid_x;
  ellipsoid_x.reserve(N*M);
  std::vector<double> ellipsoid_y;
  ellipsoid_y.reserve(N*M);
  std::vector<double> ellipsoid_z;
  ellipsoid_z.reserve(N*M);  
  
  for(int i=0;i<N*M;i++)
  {
    Eigen::VectorXd xi(6);
    xi << X(i), Y(i), Z(i), 0, 0, 0;
    xi = L * xi;
    xi = sigma * xi;
    
    auto temp = Lie::compose( Lie::SE3::Exp(xi), pose.mu() );
    Eigen::VectorXd xyz = temp.t();
    ellipsoid_x.push_back(xyz(0));
    ellipsoid_y.push_back(xyz(1));
    ellipsoid_z.push_back(xyz(2));    
  }

  
  if(name.size())
  {
    //    std::cout << "HERE: " << name << "\n";
    if(keywords.size()){
      plt::named_plot(name, ellipsoid_x, ellipsoid_y, color, keywords);
    }
    else
    {
      plt::named_plot(name, ellipsoid_x, ellipsoid_y, color);
    }
  }
  else
  {
    if(keywords.size()){    
      plt::plot(ellipsoid_x, ellipsoid_y, color, keywords);
    }
    else
    {
      plt::plot(ellipsoid_x, ellipsoid_y, color);
    }
  }
}


// template <typename StateT>
// void plot_sigma_error_ellipsoid(const Lie::UncertainGroupElementRef<Lie::SE3, StateT>& pose, double sigma=3.0, std::string color="k", std::map<std::string, std::string> keywords={}, std::string name="")
// {

//   Eigen::MatrixXd cov = pose.marginal_cov();
  
//   Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> solver(cov);

//   Eigen::VectorXd values = solver.eigenvalues();
//   Eigen::MatrixXd vectors = solver.eigenvectors();

//   std::vector<double> evalues;
//   for(int i=0;i<6;i++)
//   {
//     evalues.push_back(values(i));
//   }
//   auto indices = sort_indices(evalues);

//   //  std::cout << "\n" << values.transpose() << "\n" << vectors << "\n\n";

//   //  std::cout << indices[0] << "\n";

//   // int max_index = values(0) > values(1) ? 0 : 1;
//   // int min_index = values(0) > values(1) ? 1 : 0;
//   // std::cout << max_index << "\n";

//   // Sigma Confidence Ellipse
//   double a = sigma*sqrt(values(indices[0]));
//   double b = sigma*sqrt(values(indices[1]));
//   double c = sigma*sqrt(values(indices[2]));
//   double d = sigma*sqrt(values(indices[3]));
//   //  double major_radii = sigma*sqrt(values(max_index));
//   //  double minor_radii = sigma*sqrt(values(min_index));  


//   //  double rot_angle = atan2(vectors(1, max_index), vectors(0, max_index));
//   //  std::cout << rot_angle << "\n";

//   std::vector<double> ellipse_x;
//   std::vector<double> ellipse_y;
  
//   double PI = 3.14159265359;  
//   double step = 0.001;

//   for(int n=0;n<3;n++)
//   {
//     double angle = -PI;
//     while(angle < PI)
//     {

//       //      std::cout << n << " " << angle << "\n";
      
//       Lie::SE3::TangentVector p;
//       if(n==0)
//       {
//         p = a*vectors.col(indices[0])*sin(angle) + b*vectors.col(indices[1])*cos(angle);
//       }
//       else if(n==1)
//       {
//         p = b*vectors.col(indices[1])*sin(angle) + c*vectors.col(indices[2])*cos(angle);
//       }
//       else if(n==2)
//       {
//         p = a*vectors.col(indices[0])*sin(angle) + c*vectors.col(indices[2])*cos(angle);
//       }
      
//       auto transformed_pose = Lie::SE3::Exp(p)*pose.mu();

//       //auto loc = transformed_pose.R()*transformed_pose.t();
//       auto loc = transformed_pose.t();

//       ellipse_x.push_back(loc(0));
//       ellipse_y.push_back(loc(1));      
      
//       angle += step;
//     }
//   }

//   if(name.size())
//   {
//     //    std::cout << "HERE: " << name << "\n";
//     if(keywords.size()){
//       plt::named_plot(name, ellipse_x, ellipse_y, color, keywords);
//     }
//     else
//     {
//       plt::named_plot(name, ellipse_x, ellipse_y, color);
//     }
//   }
//   else
//   {
//     if(keywords.size()){    
//       plt::plot(ellipse_x, ellipse_y, color, keywords);
//     }
//     else
//     {
//       plt::plot(ellipse_x, ellipse_y, color);
//     }
//   }


// }

template <typename StateT>
void plot_flattened_sigma_error_ellipsoid_3dsphere(const Lie::UncertainGroupElementRef<Lie::SE3, StateT>& pose, double sigma, std::string color, std::string name)
{
  plot_flattened_sigma_error_ellipsoid_3dsphere(pose, sigma, color, {}, name);
}

// template <typename StateT>
// void plot_sigma_error_ellipsoid(const Lie::UncertainGroupElementRef<Lie::SE3, StateT>& pose, double sigma, std::string color, std::string name)
// {
//   plot_sigma_error_ellipsoid(pose, sigma, color, {}, name);
// }

void plot_xy_sigma_error_ellipse(double x, double y, const Eigen::MatrixXd& full_cov, double sigma=3.03, std::string color="k", std::string name="", std::map<std::string, std::string> keywords={})
{
  double PI = 3.14159;
  int N = 1000;
  int M = 1000;
  Eigen::VectorXd phi = Eigen::VectorXd::LinSpaced(N, -PI, PI);
  Eigen::VectorXd theta = Eigen::VectorXd::LinSpaced(M, -PI/2.0, PI/2.0);
  auto grid = mesh_grid(theta, phi);
  Eigen::MatrixXd X_sph = (grid.first.unaryExpr(&Cos) * grid.second.unaryExpr(&Cos)).matrix();
  Eigen::MatrixXd Y_sph = (grid.first.unaryExpr(&Cos) * grid.second.unaryExpr(&Sin)).matrix();
  //  Eigen::MatrixXd Z_sph = (grid.first.unaryExpr(&Sin)).matrix();

  Eigen::RowVectorXd X = Eigen::Map<const Eigen::RowVectorXd>(X_sph.data(), N*M);
  Eigen::RowVectorXd Y = Eigen::Map<const Eigen::RowVectorXd>(Y_sph.data(), N*M);
  //  Eigen::RowVectorXd Z = Eigen::Map<const Eigen::RowVectorXd>(Z_sph.data(), N*M);  
  
  Eigen::MatrixXd cov = full_cov.block(0,0,2,2);
  Eigen::LLT<Eigen::MatrixXd> lltOfCov(cov);
  auto L = lltOfCov.matrixL();

  std::vector<double> ellipse_x;
  ellipse_x.reserve(N*M);
  std::vector<double> ellipse_y;
  ellipse_y.reserve(N*M);
  // std::vector<double> ellipse_z;
  // ellipse_z.reserve(N*M);  
  
  for(int i=0;i<N*M;i++)
  {
    Eigen::VectorXd xy(2);
    xy << X(i), Y(i);
    xy = L * xy;
    xy = sigma * xy;

    ellipse_x.push_back(xy(0) + x);
    ellipse_y.push_back(xy(1) + y);
    //    ellipse_z.push_back(xyz(2));    
  }

  
  if(name.size())
  {
    //    std::cout << "HERE: " << name << "\n";
    if(keywords.size()){
      plt::named_plot(name, ellipse_x, ellipse_y, color, keywords);
    }
    else
    {
      plt::named_plot(name, ellipse_x, ellipse_y, color);
    }
  }
  else
  {
    if(keywords.size()){    
      plt::plot(ellipse_x, ellipse_y, color, keywords);
    }
    else
    {
      plt::plot(ellipse_x, ellipse_y, color);
    }
  }
}

// void plot_xy_sigma_error_ellipse(double x, double y, const Eigen::MatrixXd& full_cov, double sigma=3.0, std::string color="k", std::string name="", std::map<std::string, std::string> keywords={})
// {
//   Eigen::MatrixXd cov = full_cov.block(0,0,2,2);
  
//   Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> solver(cov);

//   Eigen::VectorXd values = solver.eigenvalues();
//   Eigen::MatrixXd vectors = solver.eigenvectors();

//   int max_index = values(0) > values(1) ? 0 : 1;
//   int min_index = values(0) > values(1) ? 1 : 0;
//   //  std::cout << max_index << "\n";

//   // Sigma Confidence Ellipse
//   double major_radii = sigma*sqrt(values(max_index));
//   double minor_radii = sigma*sqrt(values(min_index));  
//   double a = major_radii;
//   double b = minor_radii;
  
//   double rot_angle = atan2(vectors(1, max_index), vectors(0, max_index));
//   //  std::cout << rot_angle << "\n";
  
//   std::vector<double> ellipse_x;
//   std::vector<double> ellipse_y;
  
//   double PI = 3.14159265359;  
//   double step = 0.001;

//   for(int n=0;n<1;n++)
//   {
//     double angle = -PI;
//     while(angle < PI)
//     {
      
//       Eigen::VectorXd p;
//       if(n==0)
//       {
//         p = a*vectors.col(max_index)*sin(angle) + b*vectors.col(min_index)*cos(angle);
//       }
      
//       ellipse_x.push_back(p(0) + x);
//       ellipse_y.push_back(p(1) + y);      
      
//       angle += step;
//     }
//   }


//   if(keywords.size()){
//     plt::named_plot(name, ellipse_x, ellipse_y, color, keywords);
//   }
//   else if(name.size())
//   {
//     plt::named_plot(name, ellipse_x, ellipse_y, color);
//   }
//   else
//   {
//     plt::plot(ellipse_x, ellipse_y, color);
//   }
// }

}; // namespace LieUtils
#endif //__LIE_UTILS_HPP
