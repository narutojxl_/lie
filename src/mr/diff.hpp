//diff.hpp

#ifndef DIFF_HPP
#define DIFF_HPP

#include <Eigen/Core>

namespace MR
{
  namespace DIFF
  {

    typedef const Eigen::VectorXd& InputVectorRef;
    typedef Eigen::VectorXd OutputVector;
 
    Eigen::MatrixXd num_jacob_central( OutputVector (*f)(InputVectorRef, void *), 
                                       InputVectorRef x, void * args, double del=0.000001);
  };
}; 
#endif
