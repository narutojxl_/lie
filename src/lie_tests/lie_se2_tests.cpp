#include "lie/se2.hpp"

#include "gtest/gtest.h"

#define ASSERT_MATRICES_APPROX_EQ(M_actual, M_expected) \
  ASSERT_TRUE(M_actual.isApprox(M_expected, 1e-6)) << "  Actual:\n" << M_actual << "\nExpected:\n" << M_expected

TEST(SE2Test, Test1)
{
  double PI = 3.14159265359;
  Lie::SE2 H(1.0, 2.0, PI/2);

  Eigen::Matrix3d H_true;
  H_true <<
      0, -1, 1, 
      1, 0, 2, 
      0, 0, 1;

  ASSERT_MATRICES_APPROX_EQ(H(), H_true);
}
TEST(SE2Test, Test2)
{
  double PI = 3.14159265359;
  Lie::SO2 R(PI/2);
  Eigen::Vector2d t;
  t << 1.0, 2.0;
  
  Lie::SE2 H(R, t);

  Eigen::Matrix3d H_true;
  H_true <<
      0, -1, 1, 
      1, 0, 2, 
      0, 0, 1;

  ASSERT_MATRICES_APPROX_EQ(H(), H_true);
}

TEST(SE2MultTest, Test1)
{
  Lie::SE2 H1(1, 2, 0);
  Lie::SE2 H2(1, 2, 0);

  Eigen::Matrix3d H_true;
  H_true <<
      1, 0, 2, 
      0, 1, 4, 
      0, 0, 1;

  auto H = H1*H2;

  ASSERT_MATRICES_APPROX_EQ(H(), H_true);
}

TEST(SE2InvTest, Test1)
{
  Lie::SE2 H1(1, 2, 3);

  Eigen::Matrix3d H_true;
  H_true <<
      1, 0, 0, 
      0, 1, 0,
      0, 0, 1;

  auto H = H1.inverse()*H1;

  ASSERT_MATRICES_APPROX_EQ(H(), H_true);
}

TEST(ExpTest, Test1)
{
  double PI = 3.14159265359;
  Eigen::Vector3d xi_vec;
  xi_vec << 0, 0, PI;

  Lie::SE2::TangentVector xi(xi_vec);

  Eigen::Matrix3d H_true;
  H_true <<
      -1, 0, 0,
      0,  -1, 0,
      0, 0, 1;

  auto H = Lie::SE2::Exp(xi);

  ASSERT_MATRICES_APPROX_EQ(H(), H_true);
}

TEST(ExpTest, Test2)
{
  double PI = 3.14159265359;
  Eigen::Vector3d xi_vec;
  xi_vec << 3, 0, 0;

  Lie::SE2::TangentVector xi(xi_vec);

  Eigen::Matrix3d H_true;
  H_true <<
      1, 0, 3,
      0,  1, 0,
      0, 0, 1;

  auto H = Lie::SE2::Exp(xi);

  ASSERT_MATRICES_APPROX_EQ(H(), H_true);
}

TEST(ExpTest, Test3)
{
  double PI = 3.14159265359;
  Eigen::Vector3d xi_vec;
  xi_vec << 0, 6, 0;

  Lie::SE2::TangentVector xi(xi_vec);

  Eigen::Matrix3d H_true;
  H_true <<
      1, 0, 0,
      0,  1, 6,
      0, 0, 1;

  auto H = Lie::SE2::Exp(xi);

  ASSERT_MATRICES_APPROX_EQ(H(), H_true);
}

TEST(ExpLogTest, Test1)
{
  Lie::SE2 H(1, 2, 3);

  Lie::SE2 H_prime = Lie::SE2::Exp( Lie::SE2::Log( H ) );

  ASSERT_MATRICES_APPROX_EQ(H_prime(), H());
}

TEST(LogExpTest, Test1)
{
  double PI = 3.14159265359;
  
  Eigen::Vector3d xi_vec;
  xi_vec << 20, -15, PI/3;

  Lie::SE2::TangentVector xi(xi_vec);

  Lie::SE2::TangentVector xi_prime = Lie::SE2::Log(Lie::SE2::Exp(xi));

  ASSERT_MATRICES_APPROX_EQ(xi_prime, xi_vec);
}

TEST(AdjointTest, Test1)
{
  double PI = 3.14159265359;  
  Lie::SE2 T(1, 2, PI/2);

  auto Adj = Lie::SE2::Ad(T);

  Lie::Matrix3dRow Adj_true;
  Adj_true <<
      0, -1, 2,
      1,  0, -1,
      0,  0, 1;

  ASSERT_MATRICES_APPROX_EQ(Adj, Adj_true);
}
