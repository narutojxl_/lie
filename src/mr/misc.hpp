//misc.hpp

#ifndef MISC_HPP
#define MISC_HPP

#include <Eigen/Core>

double minimized_angle(double angle);

Eigen::VectorXd minimized_angle_vec(const Eigen::VectorXd& angles, bool angle_map[]);

double circular_mean (const Eigen::VectorXd& samples);

double circular_mean (const Eigen::VectorXd& samples, const Eigen::VectorXd& weights);

#endif
