//ct_util.hpp

#ifndef CT_UTIL_HPP //if ct_util.hpp hasn't been included yet ...
#define CT_UTIL_HPP

#include "eigen_util.hpp"

namespace MR
{
  namespace SO3
  {
    Eigen::MatrixXd rotxyz(double r, double p, double h);
    Eigen::MatrixXd homo4x4(const Eigen::VectorXd& X_ij);
    Eigen::VectorXd rot2rph(const Eigen::MatrixXd& R);
    //static void quat2rph(const double quat[4], double rph[3]);
  };

  namespace SSC
  {
    Eigen::VectorXd inverse(const Eigen::VectorXd& X_ij);
    Eigen::MatrixXd inverse_jac(const Eigen::VectorXd& X_ij);
    Eigen::VectorXd inverse_SE2(const Eigen::VectorXd& X_ij);
    Eigen::MatrixXd inverse_SE2_jac(const Eigen::VectorXd& X_ij);

    Eigen::VectorXd head2tail(const Eigen::VectorXd& X_ij, const Eigen::VectorXd& X_jk);
    Eigen::MatrixXd head2tail_jac(const Eigen::VectorXd& X_ij, const Eigen::VectorXd& X_jk);
    Eigen::VectorXd head2tail_SE2(const Eigen::VectorXd& X_ij, const Eigen::VectorXd& X_jk);
    Eigen::MatrixXd head2tail_SE2_jac(const Eigen::VectorXd& X_ij, const Eigen::VectorXd& X_jk);

    Eigen::VectorXd tail2tail(const Eigen::VectorXd& X_ij, const Eigen::VectorXd& X_ik);
    Eigen::MatrixXd tail2tail_jac(const Eigen::VectorXd& X_ij, const Eigen::VectorXd& X_ik);
    Eigen::VectorXd tail2tail_SE2(const Eigen::VectorXd& X_ij, const Eigen::VectorXd& X_ik);
    Eigen::MatrixXd tail2tail_SE2_jac(const Eigen::VectorXd& X_ij, const Eigen::VectorXd& X_ik);

    Eigen::VectorXd inverse_fp(const Eigen::VectorXd& X_ij, void * null);
    Eigen::VectorXd head2tail_fp(const Eigen::VectorXd& X_ij_X_jk, void * null);
    Eigen::VectorXd tail2tail_fp(const Eigen::VectorXd& X_ij_X_ik, void * null);

    Eigen::VectorXd inverse_SE2_fp(const Eigen::VectorXd& X_ij, void * null);
    Eigen::VectorXd head2tail_SE2_fp(const Eigen::VectorXd& X_ij_X_jk, void * null);
    Eigen::VectorXd tail2tail_SE2_fp(const Eigen::VectorXd& X_ij_X_ik, void * null);
  };
};
#endif
