
  Eigen::VectorXd x1(6);
  x1 << 1, 2, 3, 4, 5, 6;
  std::cout << "In1:\n" << x1 << std::endl;

  Eigen::VectorXd x2(6);
  x2 << 3.0000, 6.0000, 299.0000, 12.0000, 0.5000, 3.0000;
  std::cout << "In2:\n" << x2 << std::endl;

  Eigen::VectorXd out1;
  out1 = SSC::inverse(x1, NULL);
  std::cout << "Out1:\n" << out1 << std::endl;
  Eigen::MatrixXd J = num_jacob_central(SSC::inverse, x1, NULL);
  std::cout << "J:\n" << J << std::endl;

  Eigen::VectorXd out2;
  out2 = SSC::inverse(x2, NULL);
  std::cout << "Out2:\n" << out2 << std::endl;
  Eigen::MatrixXd J2 = num_jacob_central(SSC::inverse, x2, NULL);
  std::cout << "J:\n" << J2 << std::endl;

  std::cout << "\nHead2Tail-----\n";
  Eigen::VectorXd in1(12); in1.head(6) = x1; in1.tail(6) = x2;
  std::cout << "in1: \n" << in1 << "\n";
  out1 = SSC::head2tail(in1, NULL);
  std::cout << "Out1:\n" << out1;
  J = num_jacob_central(SSC::head2tail, in1, NULL);
  std::cout << "J:\n" << J << std::endl;

  Eigen::VectorXd in2(12); in2.head(6) = x2; in2.tail(6) = x1;
  std::cout << "in2: \n" << in2 << "\n";
  out2 = SSC::head2tail(in2, NULL);
  std::cout << "Out2:\n" << out2;
  J2 = num_jacob_central(SSC::head2tail, in2, NULL);
  std::cout << "J:\n" << J2 << std::endl;

  std::cout << "\nTail2Tail-----\n";
  in1.head(6) = x1; in1.tail(6) = x2;
  std::cout << "in1: \n" << in1 << "\n";
  out1 = SSC::tail2tail(in1, NULL);
  std::cout << "Out1:\n" << out1;
  J = num_jacob_central(SSC::tail2tail, in1, NULL);
  std::cout << "J:\n" << J << std::endl;

  in2.head(6) = x2; in2.tail(6) = x1;
  std::cout << "in2: \n" << in2 << "\n";
  out2 = SSC::tail2tail(in2, NULL);
  std::cout << "Out2:\n" << out2;
  J2 = num_jacob_central(SSC::tail2tail, in2, NULL);
  std::cout << "J:\n" << J2 << std::endl;
 
